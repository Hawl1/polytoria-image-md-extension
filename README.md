# Polytoria image markdown extension

Very basic browser extension that embeds images and links for Polytoria forums.


[My Polytoria profile](https://polytoria.com/users/18781)

### Usage:

!(img link here)

examples:

!(https://i.imgur.com/xqS3cZh.jpeg)

!(https://www.youtube.com/embed/dQw4w9WgXcQ)

include these in your posts, and this extension itll embed it

**It embeds only embeds from __only__ these sources:**
- Imgur
- YouTube (youtube.com/embed/)

If you have suggestions, please dont hesiate to tell it!
